var express = require('express');
var app = express();
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/contact', function (req, res) {
  var news=require('./news');
  news(req,res);
});

var server=app.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
   var host = process.env.IP;
  var port = server.address().port;
  console.log('Example app listening at http://%s:%s', host, port);
});
